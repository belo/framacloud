# Supprimer tous les mails de la file d'attente de postfix pour un domaine particulier

Avertissement : cette astuce ne fonctionne que si vous utilisez `postfix`. Il faudra l'adapter si vous utilisez autre chose, comme `exim` par exemple.

Il arrive qu'on ait un grand nombre de mails en attente pour un domaine erroné. Une des erreurs les plus fréquentes que nous rencontrons à Framasoft est l'utilisation d'une adresse en `@gmail.fr`. Un mail envoyé à une telle adresse n'aboutira jamais (`dig MX gmail.fr +short` vous en convaincra, il n'y a pas de champ MX pour ce domaine). Nous rencontrons aussi pas mal d'adresses `@voila.fr` alors que voila.fr a cessé de fournir du mail depuis le 12 janvier 2016 (Voir <http://korben.info/voila-cest-fini.html>).

On pourrait laisser ces mails dans la file d'attente et attendre qu'ils expirent d'eux-mêmes au bout de 5 jours… mais cela fait 5 jours où la file d'attente grossit pour rien, ce qui déclenche des alertes dans notre supervision, ce qui peut masquer un réel problème.

Pour se débarrasser de ces mails en une commande :

    mailq | grep "gmail.fr" -B1 |\
     sed -e "s@ .*@@" -e "s@--.*@@" -e "s@(\(host\|delivery\|connect\).*@@i" -e "s@\*@@g" |\
     sed ':a;N;$!ba;s/\n/ /g' |\
     sed -e "s@ \+@ -d @g" -e "s@^@postsuper -d @" -e "s@ -d \+\$@@" |\
     bash


Article initialement publié en CC-0 sur <https://fiat-tux.fr/2017/01/11/supprimer-tous-les-mails-de-la-file-dattente-de-postfix-pour-un-domaine-particulier>