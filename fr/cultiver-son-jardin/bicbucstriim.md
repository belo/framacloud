# Installation d’un serveur OPDS

Sur [Framabookin][1], nous avons mis en place un catalogue <abbr>OPDS</abbr>
contenant de nombreux ouvrages du domaine public édités par [Bibebook][2]
ainsi que l’ensemble de [nos Framabooks][3].
Ce catalogue peut être ajouté à des applications de lecture
d’<span lang="en">ebook</span> pour <span lang="en">smartphone</span>,
tablette ou liseuse afin de rechercher et consulter des ouvrages facilement.
Nous utilisons le logiciel Calibre pour préparer le catalogue
(métadonnées, couvertures, fichiers <span lang="en">epub</span>,
<abbr>cbz</abbr> et <abbr>pdf</abbr>).
C’est le dossier contenant ce catalogue qui est ensuite digéré par le
logiciel BicBucStriim pour produire le flux <abbr>OPDS</abbr> ainsi que
l’interface web mobile pour les plate-formes de lecture ne disposant pas
d’application dédiée à cette tâche.
L’ensemble est très simple à mettre en place (juste un peu fastidieux
lorsqu’il faut préparer le catalogue et ses centaines d’ouvrages)
et peut même être installé sur un serveur mutualisé.

<p class="alert alert-info">
  <span class="label label-info">Informations</span> Dans la suite de ce
  tutoriel, nous supposerons que vous avez déjà fait pointer votre nom
  de domaine sur votre serveur auprès de votre
  <a href="http://fr.wikipedia.org/wiki/Bureau_d%27enregistrement">registraire</a>,
  que vous disposez d'un serveur web et de <abbr>PHP</abbr> en version
  supérieure à 5.3.7 avec les extensions <code>mcrypt</code> et <code>sqlite3</code>.
</p>

## Installation

### 1 - Préparer la terre

![](images/icons/preparer.png)

Tout d’abord, il faut préparer le catalogue Calibre localement.
Si vous utilisez déjà Calibre pour votre usage personnel, créez une
nouvelle bibliothèque en cliquant sur l'icône présentant quelques
livres dans la barre d'outils. Le nom sous l'icône peut varier selon
votre installation : ce sera le nom du répertoire dans lequel se trouve
votre bibliothèque actuelle.
Si vous n'avez pas le bouton, ajoutez-le via *Préférences* > *Barre d'outils*

![](images/bicbucstriim/calibre_barre_outils.png)

Choisissez un dossier ou créez-en un et choisissez l'option
*Créer une bibliothèque vide à cet emplacement*.

![](images/bicbucstriim/calibre_creer_bibliotheque.png)

Si vous souhaitez utiliser la bibliothèque que vous avez créée à la
première utilisation de Calibre, le dossier des <span lang="en">ebooks</span>
se trouve à l’emplacement que vous avez choisi lors de la première
utilisation (on peut le retrouver dans *Préférences* > *Démarrer l'Assistant de bienvenue*).

![](images/bicbucstriim/calibre-assistant.jpg)

Ajoutez ensuite l’ensemble des ouvrages que vous voulez publier dans
votre catalogue et fusionnez les doublons si vos ouvrages existent dans
plusieurs formats (sélectionnez les doublons puis clic-droit
*Modifier les métadonnées* > *Fusionner les enregistrements de livres* >
*Fusionner seulement les formats dans le premier livre sélectionné - supprimer les autres* (ou `Alt+Shift+M`)).

![](images/bicbucstriim/calibre-catalogue.jpg)

### 2 - Semer

![](images/icons/semer.png)

Une fois le catalogue prêt, téléchargez la dernière version du logiciel BicBucStriim sur le
[dépôt Github officiel][4] (liens .zip ou .tar.gz).
Décompressez l’archive, renommez le dossier (par exemple `bbs`) et
uploadez le tout sur votre serveur web. Le dossier `bbs/data` doit être
accessible en écriture.
Uploadez également le dossier du catalogue Calibre et connectez-vous
sur `http://votre-site.org/bbs/admin` (identifiant et mot de passe par défaut : `admin`/`admin`).
Changez le mot de passe administrateur et dans
`Configuration` > `Chemin vers le répertoire Calibre` ajoutez
l’emplacement du dossier Calibre depuis la racine du serveur.
Exemple : `/var/www/votre-site.org/calibre/`

Pour consulter l'interface mobile, il suffit de vous rendre sur `http://votre-site.org/bbs/`.
Le flux <abbr>OPDS</abbr> se trouve sur `http://votre-site.org/bbs/opds/`

![](images/bicbucstriim/bicbucstriim.jpg)

Pour finir, juste une remarque : lorsque vous mettez à jour le catalogue
Calibre, les aperçus des livres peuvent être mal attribués ou cassés.
Pour les recréer, il suffit de supprimer les fichiers `thumb_*.png`
du dossier `bbs/data/titles/`.

<p class="text-center">
  <span class="fa fa-tree" aria-hidden="true" style="font-size:200px; color:#333"></span>
</p>

 [1]: http://framabookin.org
 [2]: http://www.bibebook.com/
 [3]: http://framabook.org
 [4]: https://github.com/rvolz/BicBucStriim/releases
