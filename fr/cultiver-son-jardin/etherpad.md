# Installation d’Etherpad

[Etherpad][2] est un logiciel libre distribué sous [licence Apache][3]
qui a pour objectif de fournir un éditeur de texte collaboratif en temps réel.
Il s’agit de la solution logicielle qui propulse le service en ligne [Framapad][4].
Du point de vue utilisateur, seul un navigateur Web est nécessaire.
Nous allons dans ce tutoriel voir la manière d’installer la partie serveur.

![](images/etherpad/framapad.jpg)

<div class="alert alert-info">
  Ce guide est prévu pour Debian Stable. À l’heure où nous écrivons ces
  mots, il s’agit de Debian 7.0 Wheezy.
  Nous partons du principe que vous venez d’installer le système et que
  celui-ci est à jour.
</div>

## 1 - Nourrir la terre

![](images/icons/preparer.png)

### NodeJS

Etherpad est une application développée avec le langage de programmation
JavaScript et s’appuie sur la plate-forme [NodeJS][6].
Malheureusement NodeJS n’est pas disponible dans les dépôts Debian par défaut.
Pour l’installer, il existe plusieurs solutions :

*   utiliser le paquet pré-compilé depuis le [site de NodeJS][7] ;
*   compiler soi-même les sources officielles ;
*   utiliser le paquet Debian issu des dépôts [backports][8].

Nous allons ici privilégier la dernière solution, car celle-ci permettra
de profiter du travail des développeurs Debian pour l’intégration du
paquet au système, les mises à jour…

Les dépôts backports, bien que non actifs par défaut lors d’une
installation de Debian, sont officiels et maintenus par des développeurs
de la distribution. Ils y proposent des milliers de logiciels en
provenance de la future version stable, et s’assurent que ceux-ci
fonctionnent correctement sur la version actuelle.

Pour activer les dépôts backports, nous allons suivre les [instructions officielles][9].

Entrons, comme administrateur du système, la commande suivante :

    echo 'deb http://http.debian.net/debian wheezy-backports main' >> \
    /etc/apt/sources.list

Elle a pour but d’ajouter la branche *main* du dépôt *wheezy-backports*
dans le fichier principal regroupant les dépôts utilisés par notre système Wheezy.
Ensuite, pour installer Node, saisissons :

    apt-get update && apt-get install nodejs

Cette commande met à jour la liste des logiciels disponibles et demande
l’installation du paquet `nodejs`.

### NPM

[NPM][10] signifie *Node Package Manager*. Il s’agit du gestionnaire de
paquets de la plate-forme NodeJS. Il nous sera indispensable pour
installer et maintenir à jour les dépendances d’Etherpad.
NPM fait partie des binaires et sources officielles lorsqu’on installe
NodeJS depuis le site Web. Mais le paquet `nodejs` en provenance des
backports Debian ne l’inclut pas.

Si le paquet [npm][11] est bien présent dans les futures versions de Debian,
il n’a en effet pas été rétroporté. Nous devrons donc l’installer manuellement.

La [procédure officielle][12] est d’utiliser la commande suivante,
comme administrateur de la machine : `curl -L https://npmjs.com/install.sh | sh`
Mais elle ne fonctionnera pas car sur une installation fraîche car `curl`
n’est pas installé par défaut et que le script recherche le binaire
`node` et non`nodejs` comme c’est le cas sous Debian.
Nous utiliserons donc, à la place (en root) :

    ln -s /usr/bin/nodejs /usr/bin/node
    apt-get install curl
    curl -L https://npmjs.com/install.sh | sh

La commande :

*   créé un lien symbolique entre le binaire `nodejs` et `node`, ayant
    pour effet que la commande `node` lance bien l’exécutable de NodeJS ;
*   installe le paquet et la commande associée `curl`, laquelle permet
    de récupérer des données à travers Internet et un grand nombre de protocoles ;
*   utilise `curl` afin de récupérer le script d’installation fourni par NPM ;
*   exécute ce dernier directement. Quelques dizaines de secondes plus
    tard, nous nous retrouvons avec la commande `npm` accessible depuis notre terminal. Elle nous sera utile pour la suite.

### Utilisateur dédié

De manière à mieux isoler l’application, nous allons créer un
utilisateur dédié à Etherpad, du même nom que le logiciel :

    useradd -mU etherpad

Cette commande crée l’utilisateur `etherpad` ainsi qu’un groupe du même nom.
Il lui assigne comme répertoire personnel `/home/etherpad`, que nous
utiliserons pour y installer l’application.

## 2 - Semer

![](images/icons/semer.png)

### Prérequis

Pour installer Etherpad, il nous faudra récupérer la dernière version du code source.
Pour cela, il est possible de télécharger l’archive correspondante ou d’utiliser `git`.
Nous privilégierons ce dernier, car cela nous permettra de gérer plus
aisément les mises à jour.
Pour installer `git`, rien de plus simple :

    apt-get install git

Certaines dépendances d’etherpad nous demanderont une phase de
compilation, automatisée.
Afin que celle-ci réussisse, nous aurons besoin du paquet `build-essential` :

    apt-get install build-essential

### Etherpad

Clonons le code source d’Etherpad dans le répertoire personnel de l’utilisateur
`etherpad` en lançant la commande suivante comme s’il s’agissait de
l’utilisateur lui-même :

    cd /home/etherpad
    su -c 'git clone https://github.com/ether/etherpad-lite.git' etherpad

Un répertoire `etherpad-lite` apparaîtra dans `/home/etherpad`.
Ensuite, nous allons lancer le script d’installation des dépendances,
lequel, en plus de vérifier et compiler si nécessaire ces dernières,
créera un fichier de configuration par défaut :

    cd /home/etherpad/etherpad-lite/
    su -c './bin/installDeps.sh' etherpad

### Configuration de base

À partir de ce moment, Etherpad est fonctionnel. Seulement, il n’y a pas
d’accès à son interface d’administration et la base de données utilisée
par défaut n’est pas vraiment taillée pour tenir la charge.
Nous nous occuperons de celle-ci plus tard.
Tâchons de créer un compte administrateur.

Pour cela, modifions le fichier `settings.json` avec notre éditeur préféré.
Par exemple avec `nano` :

    nano /home/etherpad/etherpad-lite/settings.json

Trouvons la section suivante, aux alentours de la ligne 100 :

    /* Users for basic authentication. is_admin = true gives access to /admin.
       If you do not uncomment this, /admin will not be available! */
    /*
    "users": {
      "admin": {
        "password": "changeme1",
        "is_admin": true
      },
      "user": {
        "password": "changeme1",
        "is_admin": false
      }
    },
    */

Remplaçons cette section par :

    /* Users for basic authentication. is_admin = true gives access to /admin.
       If you do not uncomment this, /admin will not be available! */
    "users": {
      "admin": {
        "password": "nøtreNouveaùMotDePasse",
        "is_admin": true
      }
    },

Enregistrons le fichier.

Récapitulons : nous avons créé un utilisateur d’Etherpad, lequel est
administrateur de la plate-forme. Il a pour identifiant `admin` et
comme mot de passe `nøtreNouveaùMotDePasse`.
Cet utilisateur servira notamment à installer des plugins pour Etherpad,
mais nous verrons cela plus loin. Toujours dans `settings.json`, autour
de la ligne 20, trouvons la ligne :

    "sessionKey": "",

Et remplissons-la par une phrase complexe que nous garderons pour nous, par exemple :

    "sessionKey": "Tp8SL85iit789qV1SxQhFjAtU4CEkmUL7WZr2DzO",

Cette clé servira pour la connexion des utilisateurs et administrateurs.
Enfin, nous souhaiterons sans doute modifier le texte par défaut
lorsqu’un nouveau pad est créé.
Pour cela, nous pourrons modifier la variable `defaultPadText`,
située en l’état à la ligne 55.

## 3 - Arroser

![](images/icons/arroser.png)

### La base de données

Par défaut, c’est [node-dirty][15], une base embarquée extrêmement
simple qui est employée. Etherpad enregistre toutes les actions en base
et cela peut rapidement représenter beaucoup de données.
Chez Framasoft, après avoir employé MySQL, puis à MongoDB, pour finalement utiliser [PostgreSQL](https://www.postgresql.org/)
qui offre de bons résultats pour cet usage.
Nous faisons donc le choix d’installer postgres et profitons d’avoir
activé les backports pour obtenir une version plus récente que celle
disponible sous Debian Stable par défaut :

    apt-get install -t wheezy-backports postgresql postgresql-client

Ensuite, créons un utilisateur et une base de données :

```
CREATE USER mon-utilisateur WITH password 'MotDePasse';
CREATE DATABASE etherpad WITH OWNER mon-utilisateur;
```

Le fichier de configuration de PostgreSQL se situe dans `/etc/postgresql/NUMERO-DE-VERSION/postgresql.conf`.
Pour un usage dédié à Etherpad, la configuration par défaut nous convient.

Nous devons maintenant répercuter l’usage de PostgreSQL dans la
configuration d’Etherpad.
Modifions le fichier `/home/etherpad/etherpad-lite/settings.json` et remplaçons :

    //The Type of the database. You can choose between dirty, postgres, sqlite and mysql
    //You shouldn't use "dirty" for for anything else than testing or development
    "dbType" : "dirty",
    //the database specific settings
    "dbSettings" : {
                     "filename" : "var/dirty.db"
                   },

Par :

    //The Type of the database. You can choose between dirty, postgres, sqlite and mysql
    //You shouldn't use "dirty" for for anything else than testing or development
    //"dbType": "dirty",
    //the database specific settings
    //"dbSettings" : {
    //                 "filename" : "var/dirty.db"
    //               },
    "dbType" : "postgrespool",
    "dbSettings" : {
                      "host"    : "localhost",
                      "port"    : 5432,
                      "password": "PASSWORD",
                      "database"  : "etherpad"
                   },

Nous avons remplacé `dirty` par `postgrespool` et avons fourni les paramètres
par défaut (excepté le nom de la base de données pour lequel nous avons choisi `etherpad` et le mot de passe).

### Lancer Etherpad

Nous pouvons maintenant lancer Etherpad par la commande :

    su -c 'sh /home/etherpad/etherpad-lite/bin/run.sh' etherpad

Elle a pour effet d’utiliser le script fourni prévu à cet effet, et
l’application sera lancée par l’utilisateur `etherpad`.
À partir de ce moment, une instance d’Etherpad fonctionne sur votre
machine et utilise la base PostgreSQL configurée.
Etherpad est accessible de l’extérieur et écoute par défaut sur le port 9001.
Nous pourrions modifier le fichier `settings.json` pour changer ce comportement.

### Procéder aux mises à jour d’Etherpad

Notre Etherpad est sorti de terre et se porte bien mais comment le faire durer ?
Voyons cela : arrêtons notre instance si elle est lancée et utilisons
`git` pour récupérer la dernière version du logiciel :

    cd /home/etherpad/etherpad-lite/
    su -c 'git pull origin' etherpad

Au prochain lancement, Etherpad vérifiera tout seul les dépendances et
les mettra à jour si nécessaire avant de poursuivre son exécution.

### Administration et plugins Etherpad

Par défaut, comme nous l’avons dit, Etherpad est lancé sur le port `9001`.
Cela signifie que nous pouvons accéder à notre instance à l’adresse : `http://notre-nom-de-domaine-ou-ip:9001`.
La partie administration se situe sur `http://notre-nom-de-domaine-ou-ip:9001/admin`.
Un identifiant et un mot de passe nous seront demandés.
Il s’agit de ceux que nous avons saisis dans le fichier `settings.json`.
L’administration contient :

*   *Plugin manager* : une interface de gestion de plugins etherpad ;
*   *Settings* : un éditeur de texte basique pour modifier le fichier `settings.json` ;
*   *Troubleshooting information* : des données pour aider à résoudre d’éventuels soucis.

Le plus intéressant est le gestionnaire de plugins, qui nous permettra
d’enrichir les fonctionnalités d’Etherpad :

![](images/etherpad/etherpad-plugins.png)

Nous y trouverons :

*   la liste des plugins installés *Installed plugins* ;
*   la liste des plugins disponibles *Available plugins*.

Nous trouverons des modules complémentaires : certains enrichissent
l’éditeur de texte, d’autres permettent de nouveaux exports ou apportent
de nouvelles briques comme la visioconférence.

## 4 - Pailler

![](images/icons/pailler.png)

### Mise en place d’un proxy inverse

Etherpad est capable de fonctionner seul sur un port standard, y compris
de prendre en charge les connexions chiffrées SSL/TLS.
Mais il est possible que nous ne dédiions pas notre machine à ce service
et que nous ayons d’autres applications Web à lui adjoindre.
Il peut dans ce cas être intéressant d’utiliser un serveur frontal qui
jouera le rôle de proxy inverse vers Etherpad.

Pour cet exemple, nous choisissons d’employer [Nginx][19] et un domaine
ou sous-domaine dédié à l’instance Etherpad.

Une fois encore, profitons des backports pour installer une version récente,
qui supportera entre autres le standard WebSocket, très utile pour
profiter pleinement du temps réel offert par Etherpad :

    apt-get install -t wheezy-backports nginx

Comme pour PostgreSQL, Nginx sera pré-configuré et lancé par défaut au
démarrage de Debian dès l’installation. Modifions la configuration
d’Etherpad de manière à ce que celui-ci ne soit plus accessible directement.

Cherchons, dans `settings.json`, la ligne suivante :

    "ip": "0.0.0.0",

Et remplaçons-la par l’adresse locale :

    "ip": "127.0.0.1",

Relançons Etherpad et passons à la configuration spécifique de Nginx.
Créons le fichier `/etc/nginx/sites-avaible/etherpad`, par exemple avec `nano` :

    nano /etc/nginx/sites-available/etherpad

À l’intérieur, nous optons pour la configuration suivante :

    server {
        listen 80;
        server_name sous-domaine.notre-nom-de-domaine;
        access_log  /var/log/nginx/etherpad.access.log;
        error_log  /var/log/nginx/etherpad.error.log;

        location / {
            proxy_pass      http://127.0.0.1:9001;
            proxy_set_header       Host $host;
            proxy_buffering off;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header Host $host;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection $connection_upgrade;
        }
    }
    map $http_upgrade $connection_upgrade {
      default upgrade;
      ''      close;
    }

Quelques explications :

*   Le port 80 est le port standard pour le Web (protocole HTTP).
    Libre à nous de configurer un accès chiffré ;
*   Le nom du serveur est le nom de domaine ou sous-domaine sur lequel
    nous souhaitons que notre instance Etherpad soit accessible ;
*   Les *_log permettent d’isoler la journalisation des accès et
    éventuelles erreurs de Nginx vers Etherpad vers des fichiers spécifiques ;
*   Tout le trafic sur ce domaine est redirigé vers l’adresse locale,
    sur le port 9001, port par défaut d’Etherpad.
*   Enfin, le dernier bloc permet d’assurer le fonctionnement du
    WebSocket pour Etherpad.

Nous allons ensuite activer cette configuration en créant un lien
symbolique vers la configuration active :

    ln -s /etc/nginx/sites-available/etherpad /etc/nginx/sites-enabled/

Relançons nginx de manière à ce qu’il prenne en compte le nouveau fichier de configuration :

    service nginx reload

Pour plus d’informations et d’autres exemples, nous nous référerons au
[wiki officiel][20] d’Etherpad.

### Autres moteurs de base de données

Etherpad supporte d’autres moteurs de base de données que postgres, via
[ueberDB][21].

Il s’agit d’une bibliothèque NodeJS spécialement développée pour Etherpad
et qui transforme des moteurs classiques en bases clés-valeurs.
Parmi les solutions supportées, l’une des plus communes est MySQL.
Si nous avions souhaité employer MySQL plutôt que PostgreSQL,
la configuration serait restée relativement simple.
Dans `settings.json`, au lieu de modifier la section `dbType` et la section `dbSettings`,
nous aurions pu nous servir de l’exemple fourni par défaut,
nous permettant de définir l’utilisateur, l’hôte, le mot de passe et
de choisir la base de données qui sera utilisée :

     /* An Example of MySQL Configuration */
    "dbType" : "mysql",
    "dbSettings" : {
      "user" : "root",
      "host" : "localhost",
      "password": "",
      "database": "store"
    },

Pour aller plus loin avec MySQL et Etherpad, nous consulterons la page de la documentation officielle qui
[y est dédiée][22].

### Concernant Framapad

Au 05/07/2017, l’ensemble du service tourne avec 7 instances hébergés sur 4 machines
différentes. Chaque instance a sa propre base de données, port d’écoute et service indépendant.
La page d’accueil n’est [une simple page HTML](https://framagit.org/framasoft/framapad)
qui sert à aiguiller l’utilisateur.

Nous utilisons  Etherpad en version 1.5.7 et avec PostgreSQL
(MongoDB posait des problèmes compte tenu du volume de pads utilisés)
avec les plugins suivants :

*   `author_hover` 0.0.17 (auteur affiché au survol de la souris)
*   `comment_page` 0.0.35 (annotation dans la marge)
*   `countable` 0.0.7 (compteur de caractères/mots/phrases)
*   `delete_empty_pads` 0.0.4 (suppression des pads jamais édités)
*   `font_color` 0.0.11 (bouton pour colorer le texte)
*   `framasoft` 0.0.3 (juste la framanav)
*   `headings2` 0.0.9 (titre)
*   `markdown` 0.0.10 (édition et export en markdown)
*   `pads_stats` 0.0.8 (nombres de pads de l’instance)
*   `page_view` 0.5.23 (options pour voir le document sous forme de page (idem, que pour l’utilisateur))
*   `prompt_for_name` 0.1.0 (demande le nom de l’auteur au bout de quelques secondes)
*   `spellcheck` 0.0.3 (vérificateur d’orthographe)
*   `subscript_and_superscript` 0.0.3 (boutons indice et exposant)
*   `table_of_content` 0.1.15 (table des matières)
*   `user_font_size` 0.0.1 (option pour changer la taille de police pour l’utilisateur pas pour le document)
*   `warn_too_much_chars` 0.0.1 (alerte quand un pad atteint les 150 000 caractères)

Sur les instances de pads à durée limitée il y a en plus
`delete_after_delay` 0.0.8 et évidement sur les pads privés il y a `mypads`.

<p class="text-center">
  <span class="fa fa-tree" aria-hidden="true" style="font-size:200px; color:#333"></span>
</p>

 [1]: http://framapad.org
 [2]: http://etherpad.org/
 [3]: http://fr.wikipedia.org/wiki/Licence_Apache
 [4]: http://framapad.org/
 [6]: http://nodejs.org/
 [7]: http://nodejs.org/download/
 [8]: http://backports.debian.org/
 [9]: http://backports.debian.org/Instructions/
 [10]: https://www.npmjs.com/
 [11]: https://packages.debian.org/search?suite=all&section=all&arch=any&searchon=names&keywords=npm
 [12]: https://github.com/npm/npm#fancy-install-unix
 [15]: https://github.com/felixge/node-dirty
 [19]: http://nginx.org/
 [20]: https://github.com/ether/etherpad-lite/wiki/How-to-put-Etherpad-Lite-behind-a-reverse-Proxy
 [21]: https://github.com/Pita/ueberDB
 [22]: https://github.com/ether/etherpad-lite/wiki/How-to-use-Etherpad-Lite-with-MySQL
