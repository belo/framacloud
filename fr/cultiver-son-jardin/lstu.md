# Installation de LSTU

[<abbr>LSTU</abbr>][1] est le logiciel raccourcisseur d’<abbr>URL</abbr> que nous proposons sur [Frama.link][2]/[Huit.re][3].

![](images/lstu/framalink.jpg)

Voici un tutoriel pour vous aider à l’installer sur votre serveur.

<p class="alert alert-info">
  <span class="label label-primary">Informations</span> Dans la suite de
  ce tutoriel, nous supposerons que vous avez déjà fait pointer votre
  nom de domaine sur votre serveur auprès de votre
  <a href="http://fr.wikipedia.org/wiki/Bureau_d%27enregistrement">registraire</a>
  et que vous disposez d’un serveur dédié sous Debian.
</p>

## Prérequis

<abbr>LSTU</abbr> est codé en Perl, pour le faire fonctionner il est
nécessaire d’installer Carton, un gestionnaire de modules Perl.

    cpan Carton

## Installation

### 1 - Préparer la terre

![](images/icons/preparer.png)

Tout d’abord, connectez-vous en tant que `root` sur votre serveur et
créez un compte utilisateur `lstu`.

    useradd lstu
    groupadd lstu
    mkdir /var/www/lstu
    chown -R lstu:lstu /var/www/lstu

### 2 - Semer

![](images/icons/semer.png)

Téléchargez les fichiers de la dernière version sur le [dépôt officiel][4]
(« Download zip » en haut à droite ou bien en ligne de commande avec `git`),
copiez son contenu dans le dossier `/var/www` et attribuez les droits
des fichiers à l’utilisateur `lstu`

    cd /var/www/
    git clone https://framagit.org/luc/lstu.git
    chown lstu:lstu -R /var/www/lstu

![](images/lstu/lstu-fichiers.jpg)

Connectez-vous avec l’utilisateur `lstu` : `su lstu -s /bin/bash`
et lancez la commande d’installation des dépendances depuis le
dossier `/var/www/lstu`

    cd /var/www/lstu
    su lstu -s /bin/bash
    carton install

Maintenant que tout est prêt, modifiez le fichier de configuration de
<abbr>LSTU</abbr> `lstu.conf` avec votre éditeur de texte préféré sur
le modèle du fichier `lstu.conf.template`.
Par défaut le logiciel est configuré pour écouter sur le port 8080 de
l’adresse 127.0.0.1 (localhost) et avec l’utilisateur `www-data` qu’il
faut donc ici remplacer par `lstu`.

    cp lstu.conf.template lstu.conf
    vim lstu.conf

#### <abbr>LSTU</abbr> en tant que service

À présent, le serveur tournera lorsque qu’on lancera en tant que
`root` cette commande :

    carton exec hypnotoad script/lstu

Pour couper le serveur :

    carton exec hypnotoad -s script/lstu

Pour éviter de devoir relancer le serveur à la main à chaque redémarrage
du serveur, on va donc lancer <abbr>LSTU</abbr> sous forme de service.
Il faut pour ça copier le script `utilities/lstu.init` dans le fichier
`/etc/init.d/lstu`, le rendre exécutable puis copier le fichier
`utilities/lstu.default` dans `/etc/default/lstu`.

    cp utilities/lstu.init /etc/init.d/lstu
    cp utilities/lstu.default /etc/default/lstu

Il faut maintenant modifier `/etc/default/lstu` pour y mettre le chemin
d’installation de notre LSTU (`/var/www/lstu` si vous n'avez pas changé
le chemin préconisé par ce tutoriel)

    vim /etc/default/lstu
    chmod +x /etc/init.d/lstu
    chown root:root /etc/init.d/lstu /etc/default/lstu

### 4 - Pailler

![](images/icons/pailler.png)

À ce stade, si tout s’est bien passé, lorsque vous exécutez la commande
`service lstu start`, <abbr>LSTU</abbr> est pleinement fonctionnel.
Vous n’avez qu’à vous rendre sur l’<abbr>URL</abbr>`http://127.0.0.1:8080`
pour pouvoir l’utiliser.

![](images/lstu/lstu-web.jpg)

Nous allons maintenant configurer <abbr>LSTU</abbr> pour le rendre
accessible depuis un nom de domaine avec Nginx (vous pouvez également
utiliser Apache ou Varnish puisque seule la fonctionnalité de proxy
inverse nous intéresse).

#### Nginx

Installez le paquet : `apt-get install nginx`
Créez le fichier de configuration de votre domaine
`/etc/nginx/sites-available/votre-nom-de-domaine` pour y mettre ceci
(en remplaçant « votre-nom-de-domaine ») et le port 8080 si vous
l’avez changé dans la configuration de <abbr>LSTU</abbr> :

    server {
        listen 80;

        server_name votre-nom-de-domaine;
        root /var/www/lstu/public;

        access_log /var/log/nginx/lstu.access.log;
        error_log /var/log/nginx/lstu.error.log;


        location ~* ^/(img|css|font|js)/ {
            try_files $uri @lstu;
            add_header Expires "Thu, 31 Dec 2037 23:55:55 GMT";
            add_header Cache-Control "public, max-age=315360000";
        }

        location / {
            try_files $uri @lstu;
        }

        location @lstu {
            proxy_pass  http://127.0.0.1:8080;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Remote-Port $remote_port;
            proxy_redirect     off;
        }
    }

Activez votre fichier :

    ln -s /etc/nginx/sites-available/votre-nom-de-domaine /etc/nginx/sites-enabled/votre-nom-de-domaine

Enfin, relancez Nginx : `service restart nginx`

### 5 - Tailler et désherber

![](images/icons/tailler.png)

La personnalisation de votre instance de <abbr>LSTU</abbr> passe par
l’édition à la main des fichiers du dossier `templates` ou `public`
pour les fichiers statiques.
Les fichiers de langues se trouvent dans le dossier `lib/LSTU/I18N`.
Pour pouvoir personnaliser et observer vos modifications en direct,
il vous faudra stopper temporairement le service `service lstu stop` et
le démarrer avec cette commande :

    carton exec morbo script/lstu

Le serveur écoutera alors sur `http://127.0.0.1:3000` mais vous pouvez
le faire écouter sur le même port qu'avec `hypnotoad` pour continuer à
passer par Nginx pendant votre développement :

    carton exec morbo script/lstu --listen=http://127.0.0.1:8080

<p class="text-center">
  <span class="fa fa-tree" aria-hidden="true" style="font-size:200px; color:#333"></span>
</p>

 [1]: https://lstu.fr
 [2]: https://frama.link
 [3]: https://huit.re
 [4]: https://framagit.org/luc/lstu
