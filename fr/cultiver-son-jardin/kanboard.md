# Installation de Kanboard

[Kanboard][1] est le logiciel de gestion de projets que nous proposons
sur [framaboard.org][2]. Voici un tutoriel pour l’installer sur votre serveur.

![](images/kanboard/framaboard.png)

<div class="alert alert-info">
  Nous considérerons dans la suite de ce tutoriel que vous disposez d’un
  serveur sous Debian 8 (Jessie) avec les logiciels Apache et PHP (5.3.3 minimum) installés.
  Si vous vous trouvez dans une autre configuration, n’hésitez pas à jeter
  un coup d’œil à <a href="https://github.com/fguillot/kanboard/blob/master/doc/index.markdown#installation">la documentation officielle</a>
  qui est très bien faite (mais en anglais).
  Nous supposerons que vous avez déjà fait pointer votre nom de domaine
  sur votre serveur auprès de votre registraire.
</div>

## Pré-requis

Vous devez d’abord installer les paquets requis par Kanboard
pour fonctionner correctement (ou vous assurer qu’ils le sont) :

    $ apt-get install -y php5-sqlite php5-gd unzip

## Installation

### Semer

![](images/icons/semer.png)

Téléchargez la dernière version de Kanboard sur [kanboard.net/downloads][3]
(le lien « Stable version »).
Décompressez l’archive et uploadez les fichiers sur votre serveur web.
Vous n’avez plus qu’à vous rendre sur `http://votre-site.org/kanboard/`.
Les identifiants par défaut sont `admin` et `admin`.
Pour fonctionner, vous devez vous assurer que le répertoire `kanboard/data`
est accessible en écriture par le serveur web.

### Tailler et désherber

![](images/icons/tailler.png)

Par défaut, les identifiants sont `admin` / `admin` :
il n’est pas recommandé de les laisser tels quels !
Une fois connecté, rendez-vous dans « Gestion des utilisateurs > admin > Changer le mot de passe ».
Vous pouvez aussi changer le nom d’utilisateur en allant sur « Modifier le profil ».
Vous pouvez désormais commencer à utiliser Kanboard !

<p class="text-center">
  <span class="fa fa-tree" aria-hidden="true" style="font-size:200px; color:#333"></span>
</p>

 [1]: http://kanboard.net/
 [2]: http://framaboard.org
 [3]: http://kanboard.net/downloads
